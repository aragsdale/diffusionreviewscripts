# DiffusionReviewScrips

This repository stores the scripts to recreate the analyses in 
"Genomic inference using forward models and the allele frequency spectrum" 
(doi: https://doi.org/10.1101/375048).


The repository should scripts to create/compute the following:

Demographic fits to the French-Canadian data and bootstrap confidence intervals (Fig 2F)

Mutation rate estimates from synonymous and intergenic data

Computing length of coding region (used in mutation rate and dfe estimates)

Cache selection spectra for DFE inference

Fit gamma distributed DFEs (Fig 3)