"""
Script to estimate mutation rates, using demographic fits from fit_FC_demography.py
"""

import moments
import numpy as np
import os, sys

# get data, if available

path_to_data = '../data/'

try:
    fs_syn = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.syn.fs'))
    fs_non = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.non.fs'))
    fs_nre = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.nre.fs'))
except IOError:
    print("Data unavailable :(")

## demographic model used in fit_FC_demography
def demo_model_misid(params, ns):
    nuB, TB, nuF, TF, p = params
    fs = moments.Demographics1D.snm(ns)
    fs.integrate([nuB],TB)
    nu_func = lambda t: [nuB * np.exp(np.log(nuF/nuB) * t/TF)]
    fs.integrate(nu_func, TF)
    return fs*(1-p) + fs[::-1]*p

## lengths of intergenic region and coding regions
Lnre = 81209832
Lcds = 33774350 # computed in length_coding_region.py

## optimal parameters from demographic fits
opt_params_nre = np.array([  5.85354433e-01,   2.47014010e-01,   2.18384868e+01,   3.09361130e-02,   3.93263594e-03])
opt_params_syn = np.array([  7.66113430e-01,   4.95981756e-01,   3.73609258e+01, 4.58147938e-02,   2.66148891e-03])

model_syn = demo_model_misid(opt_params_syn, fs_syn.sample_sizes)
model_nre = demo_model_misid(opt_params_nre, fs_nre.sample_sizes)
theta_syn = moments.Inference.optimal_sfs_scaling(model_syn, fs_syn)
theta_nre = moments.Inference.optimal_sfs_scaling(model_nre, fs_nre)

# per base theta
theta_nre /= Lnre
theta_syn /= (Lcds / 3.31)

## assume Ne = 11293, get mu
Ne = 11293
mu_nre = theta_nre / (4.*Ne)
mu_syn = theta_syn / (4.*Ne)
