"""
Script to fit DFE to French Canadian data, using the spectra cached using cache_selection_spectra.py
"""

import moments
import numpy as np
import sys, os
import scipy.stats

##########
##########

## fit to nonsynonymous data using the synonymous fit as demographic control

path_to_cached_spectra = '../data/selection_cache_syn/'

# get the cached selection spectra
spectra = {}

spectra_files = os.listdir(path_to_cached_spectra)
for fname in spectra_files:
    gamma = float(fname.split('_')[-1].split('.fs')[0])
    spectra[gamma] = moments.Spectrum.from_file(os.path.join(path_to_cached_spectra,fname))

gammas = sorted(spectra.keys())

path_to_data = '../data/'
ns = 1436
data = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.non.fs'))
# we'll fit to folded data
data = data.fold()

# get the theta from synonymous data to fix the mutation rate of nonsynonymous variants
syn_data = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.syn.fs'))
theta = moments.Inference.optimal_sfs_scaling(spectra[0], syn_data.fold())
theta_ratio = 2.31
theta_nonsyn = theta*theta_ratio

def gamma_dfe(dfe_params, ns):
    shape,scale = dfe_params
    zero_weight = scipy.stats.gamma.cdf(gammas[1],shape,loc=0,scale=scale)
    ws = [scipy.stats.gamma.pdf(gammas[ii],shape,loc=0,scale=scale) for ii in range(1,len(gammas))]
    fss = [ws[ii]*spectra[gammas[ii+1]] for ii in range(len(ws))]
    fs = zero_weight*spectra[0] + np.trapz(fss, gammas[1:], axis=0)
    return theta_nonsyn*fs

# fit shape and scale of gamma distribution to nonsynonymous data
shape,scale = (0.2,3000)
p_guess = moments.Misc.perturb_params((shape,scale))

opt_params_gamma = moments.Inference.optimize_log_fmin(p_guess, data, gamma_dfe,
                                    verbose=0,multinom=False)


model_gamma = gamma_dfe(opt_params_gamma,ns)
ll_gamma = moments.Inference.ll(model_gamma,data)

print "shape, scale of DFE (nonsyn, syn demog.):"
print opt_params_gamma
print "log-likelihood: {0}".format(ll_gamma)


##########
##########

## fits using nre-based demography

path_to_cached_spectra = '../data/selection_cache_nre'

# get cached selection spectra using demography inferred from intergenic variants
spectra = {}
spectra_files = os.listdir(path_to_cached_spectra)
for fname in spectra_files:
    gamma = float(fname.split('_')[-1].split('.fs')[0])
    spectra[gamma] = moments.Spectrum.from_file(os.path.join(path_to_cached_spectra,fname))

gammas = sorted(spectra.keys())

ns = 1436

# load the data
path_to_data = '../data/'

non_data = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.non.fs'))
non_data = non_data.fold()

syn_data = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.syn.fs'))
nre_data = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.nre.fs'))
theta_nre = moments.Inference.optimal_sfs_scaling(spectra[0], nre_data.fold())

# lengths of regions 
Lnre = 81209832
Lcds = 33774350

# AFS-estimated thetas using theta from intergenic regions and assuming same mutation rate between intergenic and coding regions
theta_cds = theta_nre * Lcds/Lnre
theta_syn = 1./3.31 * theta_cds
theta_nonsyn = 2.31/3.31 * theta_cds

# fit nonsynonymous data with intergenic demography control

def gamma_dfe(dfe_params, ns):
    shape,scale = dfe_params
    zero_weight = scipy.stats.gamma.cdf(gammas[1],shape,loc=0,scale=scale)
    ws = [scipy.stats.gamma.pdf(gammas[ii],shape,loc=0,scale=scale) for ii in range(1,len(gammas))]
    fss = [ws[ii]*spectra[gammas[ii+1]] for ii in range(len(ws))]
    fs = zero_weight*spectra[0] + np.trapz(fss, gammas[1:], axis=0)
    return theta_nonsyn*fs

shape,scale = (0.2,3000)
p_guess = moments.Misc.perturb_params((shape,scale))

opt_params_gamma = moments.Inference.optimize_log_fmin(p_guess, non_data, gamma_dfe,
                                    verbose=0, multinom=False)


model_gamma = gamma_dfe(opt_params_gamma,ns)
ll_gamma = moments.Inference.ll(model_gamma,non_data)

print "shape, scale of DFE (nonsyn, intergenic demog.):"
print opt_params_gamma
print "log-likelihood: {0}".format(ll_gamma)


# fit synonymous DFE with intergenic demographic control

def gamma_dfe_syn(dfe_params, ns):
    shape,scale = dfe_params
    zero_weight = scipy.stats.gamma.cdf(gammas[1],shape,loc=0,scale=scale)
    ws = [scipy.stats.gamma.pdf(gammas[ii],shape,loc=0,scale=scale) for ii in range(1,len(gammas))]
    fss = [ws[ii]*spectra[gammas[ii+1]] for ii in range(len(ws))]
    fs = zero_weight*spectra[0] + np.trapz(fss, gammas[1:], axis=0)
    return theta_syn*fs


shape,scale = (0.05,500)
p_guess = moments.Misc.perturb_params((shape,scale))

opt_params_gamma = moments.Inference.optimize_log_fmin(p_guess, syn_data.fold(), gamma_dfe_syn,
                                    verbose=0, multinom=False)


model_gamma = gamma_dfe_syn(opt_params_gamma,ns)
ll_gamma = moments.Inference.ll(model_gamma, syn_data)

print "shape, scale of DFE (syn, intergenic demog.):"
print opt_params_gamma
print "log-likelihood: {0}".format(ll_gamma)

