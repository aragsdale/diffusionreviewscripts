"""
compute the total length of the coding regions in the build used to annotate the FC data
to run:
python length_coding_region.py
will spit out Lcds
""" 

import numpy as np
import gzip
import os,sys

# pulled from ucsc browser for hg19
path_to_data = '../data/'
fname = 'all_genes.gz'

chroms = range(1,23)

intervals = {}
for chrom in chroms:
    intervals[chrom] = []

with gzip.open(os.path.join(path_to_data, fname)) as fin:
    for line in fin:
        line_split = line.split()
        chrom = line_split[0][3:]
        if chrom in ['M','X','Y'] or '_' in chrom:
            continue
        chrom = int(chrom)
        cdsStart = int(line_split[1])
        cdsEnd = int(line_split[2])
        if cdsStart == cdsEnd:
            continue
        else:
            intervals[chrom].append([cdsStart, cdsEnd])

for chrom in chroms:
    intervals[chrom] = sorted(intervals[chrom])


merged_intervals = {}

for chrom in chroms:
    merged_intervals[chrom] = []
    this_reg = intervals[chrom][0]
    for reg in intervals[chrom][1:]:
        if reg[0] > this_reg[-1]:
            merged_intervals[chrom].append(this_reg)
            this_reg = reg
        else:
            this_reg[-1] = max(reg[-1],this_reg[-1])
    merged_intervals[chrom].append(this_reg)


L = 0
for chrom in chroms:
    for reg in merged_intervals[chrom]:
        L += reg[1] - reg[0]

print "total length of coding regions, accounting for overlap: {0}".format(L)
