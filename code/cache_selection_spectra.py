"""
script to cache selection spectra used in inferring DFEs
"""

import moments
import numpy as np
import sys, os

min_gamma = 1e-6
max_gamma = 1000
num_gammas = 1000

## cache for French Canadian demography
## this was the demography fit in fit_FC_demography.py, now including selection

def sel_model(demo_params, ns, gamma=0, project_size=None):
    if project_size == None:
        y = moments.LinearSystem_1D.steady_state_1D(ns, gamma=gamma)
    else:
        y = moments.LinearSystem_1D.steady_state_1D(project_size, gamma=gamma)
    nuB, TB, nuF, TF = demo_params
    y = moments.Spectrum(y)
    y.integrate([nuB], TB, gamma=gamma)
    nu_func = lambda t: [nuB * np.exp( np.log(nuF/nuB) * t/TF )]
    y.integrate(nu_func, TF, gamma=gamma)
    if project_size is not None:
        y = y.project([ns])
    return y

## we cache for the following gammas (= 2*Ne*s)

gammas = np.logspace(np.log10(min_gamma), np.log10(max_gamma), num_gammas)

gammas = np.concatenate(([0],gammas,[1200,1500,2000,3000,5000,7500,10000,15000]))

# from nre fits
demo_params = opt_params = [ 0.585, 0.247, 21.8, 0.0309 ]

ns = 1436

neg_tol = -1e-12

path_to_cache = '../data/selection_cache_nre/'

for gamma in gammas:
    fs = sel_model(demo_params, ns, gamma=-gamma)
    print gamma, np.min(fs), fs[:3]
    if np.any(fs<neg_tol) or np.any(np.isnan(fs)):
        fs = sel_model(demo_params, ns, gamma=-gamma, project_size=20000)
        print gamma, np.min(fs), fs[:3]
    fs.tofile(os.path.join(path_to_cache,'spectrum_{0}_{1}.fs'.format(ns, gamma)))


# and now for demo_params from syn fits

demo_params = opt_params = [ 0.766, 0.496, 37.36, 0.0458 ]

ns = 1436

neg_tol = -1e-12

path_to_cache = '../data/selection_cache_syn/'

for gamma in gammas:
    fs = sel_model(demo_params, ns, gamma=-gamma)
    print gamma, np.min(fs), fs[:3]
    if np.any(fs<neg_tol) or np.any(np.isnan(fs)):
        fs = sel_model(demo_params, ns, gamma=-gamma, project_size=20000)
        print gamma, np.min(fs), fs[:3]
    fs.tofile(os.path.join(path_to_cache,'spectrum_{0}_{1}.fs'.format(ns, gamma)))

