"""
This script fits the single-population demographic model (Fig 2F)
The demographic model includes an extended bottleneck, followed by 
exponential growth
We include a parameter p_misid for the probability of ancestral 
state misidentification
"""

import numpy as np
import moments
import cPickle as pickle
import sys, os, math

# get data, if available

path_to_data = '../data/'

try:
    fs_syn = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.syn.fs'))
    fs_non = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.non.fs'))
    fs_nre = moments.Spectrum.from_file(os.path.join(path_to_data,'frcan.nre.fs'))
except IOError:
    print("Data unavailable :(")


## demographic model for a sample size ns
def demo_model_misid(params, ns):
    """
    params = (nuB, TB, nuF, TF, p)
    nuB : relative population size of bottleneck
    TB : time of bottleneck
    nuF : final size of population
    TF : time that the population exponentially grew from nuB to nuF
    p : probability of ancestral misidentification
    """
    nuB, TB, nuF, TF, p = params
    fs = moments.Demographics1D.snm(ns)
    fs.integrate([nuB],TB)
    nu_func = lambda t: [nuB * np.exp(np.log(nuF/nuB) * t/TF)]
    fs.integrate(nu_func, TF)
    return fs*(1-p) + fs[::-1]*p

###########
###########

## fit to synonymous data

p0 = (0.5, 0.5, 20.0, 0.05, 0.01)
p_guess = moments.Misc.perturb_params(p0)
upper_bound = [None,None,None,None,None]
lower_bound = [None,None,None,None,None]

## it is a good idea to run inference from a large number of initial guesses, to
## ensure that you haven't convereged to a local minimum 
## if you want to see outputs as the optimizer runs, set verbose=1
opt_params_syn = moments.Inference.optimize_log_fmin(p_guess, fs_syn, demo_model_misid, verbose=0,
                                                 fixed_params = [None,None,None,None,None],
                                                 upper_bound=upper_bound, lower_bound=lower_bound)

model_syn = demo_model_misid(opt_params_syn, fs_syn.sample_sizes)
ll_syn = moments.Inference.ll_multinom(model_syn,fs_syn)

print "optimal parameters (syn):"
print opt_params_syn
print "log-likelihood: {0}".format(ll_syn)


## to obtain confidence intervals, we resample the frequency spectrum
## and use moments.Godambe (Coffman et al 2016)
samples_syn = [fs_syn.sample() for ii in range(1000)] # 1000 resampled spectra
# opt_params_syn = np.array([  7.66e-01,   4.96e-01,   3.74e+01, 4.58e-02,   2.66e-03]) # our optimal parameters from above fit
theta_opt_syn = moments.Inference.optimal_sfs_scaling(demo_model_misid(opt_params_syn, fs_syn.sample_sizes), fs_syn) # optimal scaling (theta = 4*Ne*mu)
uncert_syn = moments.Godambe.GIM_uncert(demo_model_misid, samples_syn, opt_params_syn, fs_syn, False, True)

## for 95% confidence intervals, use opt_params_syn +/- 1.96 * uncert_syn[:-1]
## Note that the last value in uncert is the uncertainty on the inferred populations size scaled mutation
## rate theta, which is estimated by scaling the model (which assumes theta=1) to data

###########
###########

## fit to intergenic data (same exact procedure as for the synonymous data above)

p0 = (0.5, 0.5, 20.0, 0.05, 0.01)
p_guess = moments.Misc.perturb_params(p0)
upper_bound = [None,None,None,None,None]
lower_bound = [None,None,None,None,None]

opt_params_nre = moments.Inference.optimize_log_fmin(p_guess, fs_nre, demo_model_misid, verbose=0,
                                                 fixed_params = [None,None,None,None,None],
                                                 upper_bound=upper_bound, lower_bound=lower_bound)

model_nre = demo_model_misid(opt_params_nre, fs_nre.sample_sizes)
ll_nre = moments.Inference.ll_multinom(model_nre,fs_nre)

print "optimal parameters (nre):"
print opt_params_nre
print "log-likelihood: {0}".format(ll_nre)


samples_nre = [fs_nre.sample() for ii in range(1000)] # 1000 resampled spectra
#opt_params_nre = np.array([  5.85e-01,   2.47e-01,   2.18+01,   3.09e-02,   3.93e-03])
theta_opt_nre = moments.Inference.optimal_sfs_scaling(demo_model_misid(opt_params, fs_nre.sample_sizes), fs_nre) # optimal scaling (theta = 4*Ne*mu)
uncert_nre = moments.Godambe.GIM_uncert(demo_model_misid, samples_nre, opt_params_nre, fs_nre, False, True)


###########
###########
